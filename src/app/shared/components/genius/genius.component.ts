import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
@Component({
  selector: 'app-genius',
  templateUrl: './genius.component.html',
  styleUrls: ['./genius.component.css'],
})
export class GeniusComponent implements OnInit {
  public jogadasDaMaquina = Array<number>();

  public jogadosDoUsuario = Array<number>();

  public bloqueaBotao: boolean = true;

  private horizontalPosition: MatSnackBarHorizontalPosition = 'start';

  private verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  private botaoAcionado: any;

  private acertos: number = 0;

  abrirAlerta() {
    this._snackBar.open('Cannonball!!', 'Splash', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  public botoes: Array<any> = [
    { traducao: 'Amarelo', color: 'yellow', id: 1 },
    { traducao: 'Azul', color: 'blue', id: 2 },
    { traducao: 'Vermelho', color: 'red', id: 3 },
    { traducao: 'Verde', color: 'green', id: 4 },
  ];

  constructor(private _snackBar: MatSnackBar) {}

  @ViewChild('botaoIniciar') botaoIniciar: ElementRef;

  @ViewChild('genius') genius: ElementRef;

  ngOnInit(): void {}

  iniciar() {
    this.botaoIniciar.nativeElement.children[1].innerText = 'Aguarde...';
    this.desabilitarBotaoIniciar();
    this.iniciarContagem();
  }

  exibirMensagemVezUsuario(){
    this.mudarTexto("Sua vez!");
  }

  iniciarContagem() {
    let contagemParaIniciarOJogo = 4;
    var timer = setInterval(() => {
      contagemParaIniciarOJogo--;
      this.verificarParadaDeContagem(contagemParaIniciarOJogo, timer);
    }, 1000);
  }

  mudarTexto(texto: string) {
    this.botaoIniciar.nativeElement.children[1].innerText = texto;
  }

  verificarParadaDeContagem(contagemParaIniciarOJogo, timer) {
    if (contagemParaIniciarOJogo == 0) {
      this.mudarTexto('Valendo!');
      this.maquinaRealizarUmaJogada();
      clearInterval(timer);
      return;
    }
    this.mudarTexto('Aguarde...' + contagemParaIniciarOJogo);
  }

  maquinaRealizarUmaJogada() {
    setTimeout(() => {
      this.mudarTexto('');
      this.adicionarJogadaParaMaquina();
    }, 600);

    setTimeout(() => {
      this.desbloquearBotao();
      this.exibirMensagemVezUsuario();
    }, 1600);
  }

  adicionarJogadaParaMaquina() {
    var valorAleatoriaDeUmAteQuatro = Math.floor(Math.random() * 4 + 1);
    this.jogadasDaMaquina.push(valorAleatoriaDeUmAteQuatro);
    if (this.jogadasDaMaquina.length === 1) {
      this.acionarLegenda(valorAleatoriaDeUmAteQuatro);
    }
    this.limparJogadasUsuario();
  }

  limparJogadasUsuario() {
    this.jogadosDoUsuario = [];
  }

  adicionarJogadaParaUsuario(event: MouseEvent) {
    let cor = (event.target as any).id;
    this.alterarCorBorda(cor);
    this.limparBorda();
    this.jogadosDoUsuario.push(this.botoes.find((a) => a.color === cor).id);
    this.verificarSeJogadorPerdeu();
    this.jogarAutomatico();
    this.acrescentarAcertos(cor);

  }

  private acrescentarAcertos(cor: string) {
    let idBotaoSelecionado = this.botoes.find((x) => x.color == cor).id;
    let idUltimaJogadaDaMaquina =
      this.jogadasDaMaquina[this.jogadasDaMaquina.length - 1];

    if (idBotaoSelecionado == idUltimaJogadaDaMaquina) {
      this.acertos = this.acertos + 1;
    }
  }

  verificarSeJogadorPerdeu() {
    for (var index = 0; index < this.jogadosDoUsuario.length; index++) {
      var comparacao =
        this.jogadosDoUsuario[index] == this.jogadasDaMaquina[index];

      if (!comparacao) {
        alert('Você perdeu! Você teve' + ' ' + this.acertos + ' ' + 'acertos.');
        window.location.reload();
      }
    }
  }

  limparBorda() {
    setTimeout(() => {
      this.alterarCorBorda('black');
    }, 280);
  }

  bloquearBotao() {
    this.bloqueaBotao = true;
  }

  jogarAutomatico() {
    if (this.jogadosDoUsuario.length === this.jogadasDaMaquina.length) {
      this.bloquearBotao();
      setTimeout(() => {
        this.adicionarJogadaParaMaquina();
        this.percorrerTodasAsSequenciasRegistradas();
      }, 300);
    }
  }

  alterarCorBorda(cor: string) {
    this.genius.nativeElement.style.borderColor = cor;
  }

  percorrerTodasAsSequenciasRegistradas() {
    let quantidadePercorrida = 0;
    var percorrer = setInterval(() => {
      if (quantidadePercorrida > this.jogadasDaMaquina.length - 1) {
        this.desbloquearBotao();
        this.exibirMensagemVezUsuario();
        clearInterval(percorrer);
      } else {
        this.acionarLegenda(this.jogadasDaMaquina[quantidadePercorrida]);
        quantidadePercorrida++;
      }
    }, 1000);
  }

  desbloquearBotao() {
    this.bloqueaBotao = false;
  }

  acionarLegenda(valorAleatoriaDeUmAteQuatro) {

    let botao = this.botoes.find((a) => a.id === valorAleatoriaDeUmAteQuatro);
    this.mudarTexto(botao.traducao);
    this.alterarCorBorda(botao.color);
    this.habilitarBotao(botao.color);

    setTimeout(() => {
      this.mudarTexto('');
      this.alterarCorBorda('black');
      this.desabilitar(botao.color);
    }, 500);
  }

  private habilitarBotao(id: string) {

    let arr = Array.prototype.slice.call(this.genius.nativeElement.children);
    let element = arr.find((x) => x.id == id);
    this.botaoAcionado = element;
    this.botaoAcionado.classList.remove(id + '-disabled');
    this.botaoAcionado.classList.add(id);
  }

  private desabilitar(id: string) {
    this.botaoAcionado.classList.remove(id);
    this.botaoAcionado.classList.add(id + '-disabled');
  }

  desabilitarBotaoIniciar() {
    this.botaoIniciar.nativeElement.children[0].setAttribute(
      'class',
      'disabled'
    );
  }
}
